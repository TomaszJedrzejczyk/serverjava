package sda_group;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    public static final int PORT = 9002;
    public static String hostmane = "localhost";

    public static void main(String[] args) {

        try (Socket socket = new Socket(hostmane, PORT)) {

            PrintWriter printWriter = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            String inLine = input.readLine();
            System.out.println("RECEIVED: " + inLine);

            Scanner scanner = new Scanner(System.in, "UTF-8");
            String messageToServer = null;
            while (scanner.hasNext()) {
                messageToServer = scanner.nextLine();
                break;
            }
            printWriter.println(messageToServer);

            inLine = input.readLine();
            System.out.println("RECEIVED: "+inLine);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
