package sda_group;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

/**
 * Hello world!
 */
public class App {
    public static final int PORT = 9002;
    public static String hostmane = "locatlhost";

    public static void main(String[] args) {
        try (ServerSocket serverSocket = new ServerSocket(PORT)) {
            Socket connection = serverSocket.accept();
            InputStream inputStream = connection.getInputStream();
            OutputStream outputStream = connection.getOutputStream();


            Scanner scanner = new Scanner(inputStream, "UTF-8");
            PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(outputStream, "UTF-8"), true);
            printWriter.println("Hello! Please enter you name: ");

            while (scanner.hasNext()) {
                String message = scanner.nextLine();
                System.out.println("Received message form client " + message);
                printWriter.println("Hi " + message + "!");

                if (message.equalsIgnoreCase("stop"));
                System.out.println("Received stop command");
                break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
